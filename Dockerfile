FROM nginx:alpine
COPY ./public /usr/share/nginx/html
#COPY nginx.conf /etc/nginx/
EXPOSE 9090
CMD ["nginx", "-g", "daemon off;"]
